mod huffman;

use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::time::*;

fn main() {
    let mut file = File::open("enwik8").unwrap();
    let mut original = String::new();

    file.read_to_string(&mut original).unwrap();
    println!("Finished reading file");

    let mut pr = huffman::PriorityQueue::new();

    // ***********************************

    let t = Instant::now();
    for x in original.as_bytes() {
        pr.push(huffman::MinHeapNode::new(Some(*x as char), 1));
    }
    pr.sort();
    println!("Finished priority queue in: {:?}", Instant::now() - t);

    // ***********************************

    let t = Instant::now();
    while pr.queue.len() != 1 {
        let left = pr.pop();
        let right = pr.pop();

        let mut top = huffman::MinHeapNode::new(None, left.freq + right.freq);

        top.left = Some(Box::new(left));
        top.right = Some(Box::new(right));

        pr.push(top);
    }
    println!("Finished creating huffman tree in: {:?}", Instant::now() - t);

    // ***********************************

    let mut map = HashMap::new();

    let t = Instant::now();
    huffman::print_codes(&mut pr.top(), "".to_string(), &mut map);
    println!("Finished printing codes in: {:?}", Instant::now() - t);

    // ************************

    println!("{:#?}", map);
    let mut v = 0;
    let mut b = 0;

    for x in original.as_bytes() {
        let f = map.get(&x.to_string().pop().unwrap()).unwrap();
        v += *f as u64;
        b += *x as u64;
    }

    println!("Entropy: {}", v);

    let a = b as f64 / v as f64;
    println!("Compression ratio: {}", a);
}
