use std::rc::Rc;
use std::char;
use std::collections::HashMap;

//const MAX_TREE_HT: i8 = 100;

#[derive(Clone, Debug)]
pub struct MinHeapNode {
    pub data: Option<char>,
    pub freq: i64,
    pub code: u32,
    pub left: Option<Box<MinHeapNode>>,
    pub right: Option<Box<MinHeapNode>>
}

impl PartialEq for MinHeapNode {
    fn eq(&self, c: &MinHeapNode) -> bool{
        self.data == c.data && self.data.is_some()
    }

}

impl MinHeapNode {
    pub fn new(data: Option<char>, freq: i64) -> MinHeapNode{

        MinHeapNode {
            data: data,
            freq,
            code: 0,
            left: None,
            right: None,
        }
    }
}

#[derive(Debug)]
pub struct PriorityQueue {
    pub queue: Vec<MinHeapNode>
}

impl PriorityQueue {
    pub fn new() -> PriorityQueue {
        PriorityQueue {
            queue: Vec::new()
        }
    }

    pub fn sort(&mut self) {

        //self.queue.sort_unstable_by(|a, b| b.freq.cmp(&a.freq));
    }

    pub fn push(&mut self, m: MinHeapNode) {
        if self.queue.contains(&m) {
            for e in self.queue.iter_mut() {
                if e == &m {
                    e.freq += 1;
                    break
                }
            }
        } else {
            self.queue.push(m);
        }
        self.queue.sort_unstable_by(|a, b| b.freq.cmp(&a.freq));
    }

    pub fn pop(&mut self) -> MinHeapNode {
        self.queue.pop().unwrap()
    }

    pub fn top(&mut self) -> &mut MinHeapNode {
        &mut self.queue[0]
    }


}

pub fn print_codes(n: &mut MinHeapNode, mut s: String, map: &mut HashMap<char, char>) {
    if n.left.is_none() && n.right.is_none() {
        let x = u32::from_str_radix(&s, 2).unwrap();

        let b = char::from_u32(x).unwrap();

        map.insert(n.data.unwrap(), b);

        // Uncomment below to debug the codes created
        //println!("{} = {} -> {} ({})", n.data.unwrap(), n.data.unwrap() as u8, b, s);
    }

    let mut l = s.clone();

    if n.left.is_some() {
        s.push('0');
        print_codes(&mut n.left.clone().unwrap(), s, map);
    }

    if n.right.is_some() {
        l.push('1');
        print_codes(&mut n.right.clone().unwrap(), l, map);
    }

}
